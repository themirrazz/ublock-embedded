# class uBlockOrigin
[back](./README.md)

```ts
checkURL(url: string): {
    matched: boolean
    filterList?: string
}
```

Returns information about if a URL is blocked. filterList is the name of the list with a match.

```ts
addFilter(title: string, filter: string): void
```
Adds a filter.

```ts
removeFilter(title:string): void
```

Removes an added filter.
