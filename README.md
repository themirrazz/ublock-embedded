# uBlock Embedded

## What is this?
This is a fork of uBlock Origin. However, this is not a standalone extension. This is a library used for porting uBlock Origin to platforms normally not supported.
This is a JavaScript library that has been recompiled to run with compatibility support (see [compatibility.md](compatibility.md))

## Documentation
To view documentation, see [docs/README.md](docs/README.md)
