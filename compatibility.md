# uBlock Embedded Compatibility
[go home](https://gitlab.com/themirrazz/ublock-embedded)

Firefox 2
Internet Explorer 5.5+
Chrome 1+
Safari 1.5+
Samsung Internet 1+
Border 1.10+
Edge Legacy 12+
Netscape Navigator 9+
Opera 7+
NodeJS 0.10.0+
Deno 1.0+
Mirrazz Web Browser 1+
